package main

import (
	"fmt"
	//"../../guardian"
	"reflect"
)


/* this file shows how to using guardian lib to
   implement your own guardian */


func MyMissionMethod(a string){
	fmt.Println("hello, world, this is my mission.")
	fmt.Printf("and this is my params: %s \n", a)
}

func CallMethod(method interface{}){
	// here method is a interface which is a type of func
	fv := reflect.ValueOf(method)
	args := []reflect.Value{reflect.ValueOf("金天")}
	fv.Call(args)

}

func main() {
	fmt.Println("Hello, My Guardian.")


	//myMissionMethod := MyMissionMethod
	//fv := reflect.ValueOf(myMissionMethod)
	//fmt.Println(fv)
	//args := []reflect.Value{reflect.ValueOf("金天")}
	//
	//repeatDay := []string{"12:00", "14:00"}
	//repeatWeek := []int{2, 4, 5}
	//repeatMonth := []int{3, 4, 5}
	//strategy := guardian.NewStrategy("Hello", repeatDay, repeatWeek, repeatMonth)
	//mission := guardian.NewMission(strategy, fv, args,"hello", true)
	//mission.Execute()

	mission := MyMissionMethod
	CallMethod(mission)


}
