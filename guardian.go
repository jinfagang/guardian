package guardian

import (
	"log"
	"fmt"
	"reflect"
)

func init() {
	log.SetPrefix("[main center]")
	log.SetFlags(log.Ldate | log.Lshortfile)
}

type Execute interface {
	Execute()
}

type Strategy struct {
	TypicalName string
	RepeatDay []string
	RepeatWeek []int
	RepeatMonth []int
}

type Mission struct {
	MissionStrategy Strategy
	MissionMethod reflect.Value
	MissionMethodArgs []reflect.Value
	MissionLabel string
	IsDaemon bool
}


func (mission Mission) Execute() {
	fv := mission.MissionMethod
	fv.Call(mission.MissionMethodArgs)
	fmt.Println("mission executed!!")
}

func NewStrategy(typicalName string, repeatDay []string, repeatWeek []int, repeatMonth []int) *Strategy{
	return &Strategy{
		TypicalName:typicalName,
		RepeatDay:repeatDay,
		RepeatWeek:repeatWeek,
		RepeatMonth:repeatMonth,
	}
}

func NewMission(strategy *Strategy, methodName reflect.Value, args []reflect.Value, label string, isDaemon bool) *Mission{
	return &Mission{
		MissionStrategy:*strategy,
		MissionLabel:label,
		MissionMethodArgs:args,
		MissionMethod:methodName,
		IsDaemon:isDaemon,
	}
}



func guardian() {
	fmt.Println("hello, Go lang.")


}
