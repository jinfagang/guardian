package missions

import (
	"log"
	"../common"
)


func ChangeWallpaper(){
	log.Println("Changed wall paper.")

	// this is the mission strategy defines here,
	// can be individualise by personal
	strategy := common.Strategy{
		TypicalName: "ChangeWPStrategy",
		RepeatDay: []string{"8:00", "12:00"},
		RepeatWeek: []int{1, 2, 3, 5},
		RepeatMonth: []int{3, 6, 9, 10},
	}
	log.Println(strategy.RepeatDay)
}